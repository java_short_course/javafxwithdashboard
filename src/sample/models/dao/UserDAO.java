package sample.models.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.models.dto.User;

import java.util.ArrayList;

public class UserDAO {

    public ObservableList<User> users = FXCollections.observableArrayList();

    public void addUser(User user) {
        users.add(user);
    }

    public void showUsers() {
        System.out.println(users);
    }

}
