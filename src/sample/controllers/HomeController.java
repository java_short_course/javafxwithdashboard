package sample.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class HomeController {
    @FXML
    public TextField nameTextField;
    @FXML
    private Label resultLabel;

    @FXML
    private BorderPane mainLayout;

    public void showMainScreen() {
        try {
            mainLayout = FXMLLoader.load(getClass().getResource("../views/HomeView.fxml"));
            Scene scene = new Scene(mainLayout);
            Stage primaryStage = new Stage();
            primaryStage.setScene(scene);
            primaryStage.setTitle("Hello World");
            primaryStage.show();
           loadSubView("../views/UserView.fxml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadSubView(String location) {
        try {
            mainLayout.setCenter(new FXMLLoader().load(getClass().getResource(location)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void showOtherScreenButtonPressed() {
       loadSubView("../views/OtherView.fxml");
    }

    @FXML
    public void showUserScreenButtonPressed() {
        loadSubView("../views/UserView.fxml");
    }
}
