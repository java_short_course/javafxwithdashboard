package sample.controllers;

import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import sample.models.dao.UserDAO;
import sample.models.dto.User;

public class UserController  {

    @FXML
    private JFXTextField userTextField;
    @FXML
    private JFXPasswordField passwordField;

    private UserDAO userDAO = new UserDAO();

    @FXML private JFXListView<String> listView;




    @FXML
    void plaokButtonPress() {
        if (!userTextField.getText().trim().isEmpty() && !passwordField.getText().trim().isEmpty()) {
            User user = new User(userTextField.getText(), passwordField.getText());
            userDAO.addUser(user);
        } else {

            System.out.println("You cannot add user ");
        }
    }

    @FXML
    void showUsers() {
        userDAO.showUsers();
        if (!listView.getItems().isEmpty())
            listView.getItems().clear();


        for(User user: userDAO.users){
            listView.getItems().addAll(user.getUsername()+"\t"+user.getPassword());
        }
//        userDAO.users.forEach(username -> {
//            listView.getItems().addAll(username.getUsername() + " " + username.getPassword());
//        });
    }


}
